import { takeEvery, call, put, cancel, all } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import API from '../api';
import * as actions from '../actions';

function* watchFetchDrone(action) {
    let isFirst = true;

    while (true) {
        const { error, data } = yield call(API.findDroneMetrics);
        if(!isFirst) {
            yield delay(3000);
        }
        isFirst = false;
        if (error) {
            yield put({ type: actions.API_ERROR, code: error.code, message: error.message});
            yield cancel();
            return;
        }
        yield put({ type: actions.DRONE_DATA_RECEIVED, data });
    }
}

function* watchAppDroneLoad() {
    yield all([takeEvery(actions.FETCH_DRONE, watchFetchDrone)]);
}

export default [watchAppDroneLoad];