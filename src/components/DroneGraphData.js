import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import LinearProgress from '@material-ui/core/LinearProgress';
import Plot from 'react-plotly.js';

class DroneGraphData extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            sine: {
                x: [moment().valueOf()],
                y: [0],
                type: 'scatter',
                mode: 'lines',
                line: {
                    shape: 'spline',
                    smoothing: 1.3,
                    color: "rgb(0,113.985,188.955)"
                },
                "name": "temperature",
                xaxis: 'timestamp',
                yaxis: 'temperature',
                "visible": true,
                "showlegend": true
            },
            revision: 0,
            layout: {
                width: 1000,
                datarevision: 0,
                title: 'Drone data Plot',
                yaxis: {
                    title: 'temperature',
                    range: [240, 320],
                    //autorange: true,
                    rangemode: "nonnegative"
                },
                xaxis: {
                    title: 'time',
                    rangemode: "nonnegative",
                    autorange: true,
                    //showline: true,
                    type: 'date',
                    tickvals: '',
                    ticktext: '',
                    tickmode: 'array',
                    nticks: 5,
                    tick0: 1,
                    dtick: 6 * 10 * 1000,
                    tickformat: '%H:%M'
                }
            }
        };
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        let { sine, revision, layout } = prevState;

        if (!nextProps.timestamp) {
            return prevState;
        }

        if (sine.x.length > 5) {
            //sine.x
        }

        //sine.x.push(moment(nextProps.timestamp).format('Y-MM-DD hh:mm'));
        sine.x.push(nextProps.timestamp);
        sine.y.push(parseFloat(nextProps.temperature));

        revision = revision + 1;
        layout.datarevision = revision;

        layout.xaxis.tickvals = nextProps.timestamp;
        //layout.xaxis.ticktext = moment(nextProps.timestamp).format('hh:mm');
        layout.xaxis.ticktext = nextProps.timestamp;
        //layout.xaxis.tick0 = moment(nextProps.timestamp).format('hh:mm');

        return {
            ...prevState,
            revision
        }
    }

    render() {
        const { loading, temperature, timestamp } = this.props;
        if (loading) {
            return <LinearProgress />;
        }

        console.log(this.state.layout.xaxis);

        let config = {
            scrollZoom: true,
            showLink: true
        };

        return <Plot data={[this.state.sine]} layout={this.state.layout} config={config} graphDiv="graph"/>
    }
}

const mapState = (state, ownProps) => {
    const { loading, temperature, timestamp } = state.drone;
    return {
        loading,
        temperature: temperature.toFixed(2),
        timestamp
    };
};

export default connect(
    mapState,
    null
)(DroneGraphData);