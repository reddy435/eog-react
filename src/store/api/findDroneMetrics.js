import "isomorphic-fetch";

const findDroneMetrics = async () => {
    // Using the create-react-app's proxy for CORS issues
    const response = await fetch(
        `https://react-assessment-api.herokuapp.com/api/drone`
    );
    
    if (!response.ok) {
    	console.log(response);
        return { error: { code: response.status, message: response.statusText } };
    }
    const json = await response.json();
    return { data: json };
};

export default findDroneMetrics;