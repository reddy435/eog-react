import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../store/actions';
import LinearProgress from '@material-ui/core/LinearProgress';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';


class Drone extends Component {
    componentDidMount() {
        this.props.onLoad();
    }
    render() {
        const { loading, temperature, longitude, latitude, lastCheckedInSec} = this.props;

        
        if (loading) {
            return <LinearProgress />;
        }

        return (
            <div>
         <ExpansionPanel
              expanded={true}>
              <ExpansionPanelSummary>
                <Typography variant="h4">Data Visualisation</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
               <List>
                <ListItem>
                  <ListItemText primary={`Temperature: ${temperature}°F `} />
                </ListItem>
                <ListItem>
                  <ListItemText primary={`Longitude: ${longitude}`}  />
                </ListItem>
                <ListItem>
                  <ListItemText primary={`Latitude: ${latitude}`}  />
                </ListItem>
                <ListItem>
                  <ListItemText primary={`Last Received: ${lastCheckedInSec} seconds ago`}  />
                </ListItem>
              </List>
              </ExpansionPanelDetails>
            </ExpansionPanel>        
      </div>
        );
    }
}

const mapState = (state, ownProps) => {
    const { loading, temperature, longitude, latitude, lastCheckedInSec } = state.drone;
    return {
        loading,
        temperature,
        longitude,
        latitude,
        lastCheckedInSec
    };
};

const mapDispatch = dispatch => ({
    onLoad: () =>
        dispatch({
            type: actions.FETCH_DRONE
        })
});

export default connect(
    mapState,
    mapDispatch
)(Drone);