import weatherReducer from "./Weather";
import droneReducer from './Drone';

export default {
    weather: weatherReducer,
    drone: droneReducer 
};