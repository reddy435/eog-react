import * as actions from '../actions';
import moment from 'moment';
import { toast } from "react-toastify";

const initialState = {
    loading: false,
    temperature: 0,
    lastCheckedInSec: 0,
    timestamp: 0,
    latitude: null,
    longitude: null
};

const startLoading = (state, action) => {
    return { ...state, loading: true };
};

const droneDataRecevied = (state, action) => {
    
    const { data } = action;
    if (!data) {
        return state;
    }
    // take the last & latest element 
    const droneData = data.data[data.data.length-1];
    const {latitude, longitude, metric, timestamp} = droneData;
    const lastCheckedInSec = moment(moment().diff(moment(timestamp))).format('s');
    
    return {
        ...state,
        loading: false,
        latitude,
        longitude,
        timestamp,
        lastCheckedInSec,
        temperature: metric
    };
};

const apiErrorOccured = (state, action) => {
    toast.error(`Drone data not available`);
    return state;
};

const handlers = {
    [actions.FETCH_DRONE]: startLoading,
    [actions.DRONE_DATA_RECEIVED]: droneDataRecevied,
    [actions.API_ERROR]: apiErrorOccured
};

export default (state = initialState, action) => {
    const handler = handlers[action.type];
    if (typeof handler === 'undefined') return state;
    return handler(state, action);
};