import React, { Component } from 'react';
import { connect } from 'react-redux';
import LinearProgress from '@material-ui/core/LinearProgress';
import { Map, TileLayer, Marker } from 'react-leaflet';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';

class DroneMap extends Component {

    render() {
        const { loading, markerPosition } = this.props;
        if (loading) return <LinearProgress />;

        return (
            <div>
	            <ExpansionPanel
		          expanded={true}>
		          <ExpansionPanelSummary>
		            <Typography variant="h3">Map Visualisation</Typography>
		          </ExpansionPanelSummary>
		          <ExpansionPanelDetails>
		            <Map
				        center={[29.760450, -95.369781]}
				        minZoom={4}
				        zoom={4} >
				        <TileLayer
				          url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
				        />
				        <Marker position={markerPosition}></Marker>
				      </Map>
		          </ExpansionPanelDetails>
		        </ExpansionPanel>		        
      		</div>
        );
    }
}

const mapState = (state, ownProps) => {
    const { loading, longitude, latitude } = state.drone;
    return {
        loading,
        markerPosition: [latitude, longitude]
    };
};


export default connect(
    mapState,
    null
)(DroneMap);